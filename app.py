from flask import Flask, render_template
import requests
from datetime import datetime
import os

app = Flask(__name__)

@app.route('/')
def index():
    user_info = requests.get('https://api.lanyard.rest/v1/users/925855840022442004').json()
    image = 'https://api.lanyard.rest/925855840022442004.png'
    return render_template('index.html', username=user_info['data']['discord_user']['username'], status=user_info['data']['activities'][0]['state'], online=user_info['data']['discord_status'], image=image)

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/projects')
def projects():
    return render_template('projects.html')

@app.route('/devices')
def devices():
    return render_template('devices.html')

@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/security')
def security():
    return render_template('security.html')

@app.route('/extras')
def extras():
    return render_template('extras.html')

@app.route('/blog')
def blog():
    ret=""
    for post in os.listdir('./static/blog'):
        ret += f"<p><a href='/blog/{post}'>{post}</a></p>\n\n"
    if ret == "":
        return "No posts found."
    else:
        return ret

@app.route('/blog/<filename>')
def post(filename):
    return open(f"./static/blog/{filename}")

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404